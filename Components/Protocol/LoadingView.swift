public protocol LoadingView {
    
    func startAnimating()
    func stopAnimating()
}
