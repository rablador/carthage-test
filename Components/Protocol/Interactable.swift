public protocol Interactable: class {

    var isInteractable: Bool { get set }
}
