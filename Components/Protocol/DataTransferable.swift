import Foundation

public protocol DataTransferable {

    associatedtype Data

    var data: Data { get set }
}
