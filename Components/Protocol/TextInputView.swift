public protocol TextInputView {

    var isFirstResponder: Bool { get }
}
