import UIKit

public class GroupedDataSource<T>: NSObject, UITableViewDataSource {

    public typealias CellCreate = (T, UITableView, IndexPath) -> UITableViewCell

    public let groups: [Group<T>]
    public var cellForItem: CellCreate

    public init(groups: [Group<T>], cellForItem: @escaping CellCreate) {
        self.groups = groups
        self.cellForItem = cellForItem
    }

    public func item(at indexPath: IndexPath) -> T {
        return groups[indexPath.section].items[indexPath.row]
    }

    public func numberOfSections(in tableView: UITableView) -> Int {
        return groups.count
    }

    public func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return groups[section].title
    }

    public func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return groups[section].items.count
    }

    public func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let group = groups[indexPath.section]
        let item = group.items[indexPath.row]
        let cell = cellForItem(item, tableView, indexPath)
        return cell
    }
}
