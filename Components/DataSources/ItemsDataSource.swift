import UIKit

public class ItemsDataSource<Item>: NSObject, UITableViewDataSource {

    public typealias CellCreate = (Item, UITableView, IndexPath) -> UITableViewCell
    public typealias CellConfigure = (UITableViewCell, IndexPath) -> Void

    public let items: [Item]
    public let cellForItem: CellCreate
    public let configure: CellConfigure?

    public init(items: [Item], cellForItem: @escaping CellCreate, configure: CellConfigure? = nil) {
        self.items = items
        self.cellForItem = cellForItem
        self.configure = configure
    }

    public func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return items.count
    }

    public func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let item = items[indexPath.row]
        let cell = cellForItem(item, tableView, indexPath)
        configure?(cell, indexPath)
        return cell
    }
}
