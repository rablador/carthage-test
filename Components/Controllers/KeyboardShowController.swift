import UIKit

public class KeyboardShowController: NSObject {

    private let dependent: UIView?
    private let textView: UIView & TextInputView

    private var view: UIView {
        return dependent ?? textView
    }

    public init(textView: UIView & TextInputView, dependent: UIView? = nil) {
        self.textView = textView
        self.dependent = dependent
        super.init()

        setupKeyboardObservers()
    }

    public func setupKeyboardObservers() {
        let defaultCenter = NotificationCenter.default
        defaultCenter.addObserver(self,
                                  selector: #selector(keyboardWillShow(notification:)),
                                  name: NSNotification.Name.UIKeyboardWillShow,
                                  object: nil)

        defaultCenter.addObserver(self,
                                  selector: #selector(keyboardWillHide(notification:)),
                                  name: NSNotification.Name.UIKeyboardWillHide,
                                  object: nil)
    }

    private func parse(notification: NSNotification) -> (CGRect, TimeInterval, UIViewAnimationOptions)? {
        let userInfo = notification.userInfo
        guard
            let keyboardFrame = (userInfo?[UIKeyboardFrameEndUserInfoKey] as? CGRect),
            let duration = userInfo?[UIKeyboardAnimationDurationUserInfoKey] as? TimeInterval,
            let curve = userInfo?[UIKeyboardAnimationCurveUserInfoKey] as? UInt
            else { return nil }

        return (keyboardFrame, duration, UIViewAnimationOptions(rawValue: curve))
    }

    @objc
    private func keyboardWillShow(notification: NSNotification) {
        guard
            let (keyboardFrame, duration, curve) = parse(notification: notification),
            let window = view.window,
            textView.isFirstResponder
            else { return }

        let frameInContainer = view.convert(view.bounds, to: window)
        if keyboardFrame.intersects(frameInContainer) {
            let margin: CGFloat = 8.0
            let difference = abs(frameInContainer.maxY - keyboardFrame.minY) + margin
            UIView.animate(withDuration: duration, delay: 0, options: curve, animations: moveContainer(yOffset: difference))
        }
    }

    private func moveContainer(yOffset: CGFloat?) -> Callback {
        return {
            guard let yOffset = yOffset else { return }
            self.view.window?.frame.origin.y -= yOffset
        }
    }

    @objc
    private func keyboardWillHide(notification: NSNotification) {
        guard
            let (_, duration, curve) = parse(notification: notification)
            else { return }

        UIView.animate(withDuration: duration, delay: 0, options: curve, animations: { self.view.window?.frame.origin.y = 0 })
    }
}

extension UITextView: TextInputView {}
extension UITextField: TextInputView {}
