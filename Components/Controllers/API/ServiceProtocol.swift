public typealias Headers = [String: String]
public typealias JSONDictionary = [String: Any]
public typealias Service = PostServiceProtocol & GetServiceProtocol & PutServiceProtocol

public protocol PostServiceProtocol {

    func post<T: Decodable>(path: String, headers: Headers?, params: JSONDictionary, completion: @escaping ResultHandler<T>)
}

public protocol GetServiceProtocol {

    func get<T: Decodable>(path: String, headers: Headers?, params: JSONDictionary, completion: @escaping ResultHandler<T>)
}

public protocol PutServiceProtocol {

    func put(path: String, headers: Headers?, params: JSONDictionary, completion: @escaping ResultHandler<Void>)
}
