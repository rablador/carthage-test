import UIKit

extension UITextField {

    public func addDoneButton() {
        let toolbar = UIToolbar()
        toolbar.sizeToFit()

        let doneButton = UIBarButtonItem(title: "Done", style: .done, target: self, action: #selector(donePressed))
        let spaceing = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
        toolbar.items = [spaceing, doneButton]
        inputAccessoryView = toolbar
    }

    @objc
    private func donePressed() {
        resignFirstResponder()
    }
}
