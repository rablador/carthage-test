public class Server {

    private (set) var host: String
    private (set) var authorization: String

    public var isProduction: Bool {
        return self === production
    }

    fileprivate init(host: String, authorization: String) {
        self.host = host
        self.authorization = authorization
    }

    public init(server: Server) {
        self.host = server.host
        self.authorization = server.authorization
    }

    public func update(to server: Server) {
        host = server.host
        authorization = server.authorization
    }
}

public var production: Server {
    get {
        guard let production = APIConfig.shared.production else { fatalError("APIConfig needs a 'production' variable to be set.") }
        return production
    }
}

public var test: Server {
    get {
        guard let test = APIConfig.shared.test else { fatalError("APIConfig needs a 'test' variable to be set.") }
        return test
    }
}
