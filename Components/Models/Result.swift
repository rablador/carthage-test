public typealias ResultHandler<T> = (Result<T>) -> Void

public enum Result<T> {
    
    case success(T)
    case failure(error: Error?)
}

extension Result {

    public func handle(success: ValueCallback<T>?, failure: ValueCallback<Error?>) {
        switch self {
        case .success(let data):
            success?(data)
        case .failure(error: let error):
            failure(error)
        }
    }
}
