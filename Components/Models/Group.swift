public struct Group<T> {

    public var title: String
    public var items: [T] = []
}
