extension UIRefreshControl: LoadingView {

    public func stopAnimating() {
        endRefreshing()
    }

    public func startAnimating() {
        beginRefreshing()
    }
}
