import UIKit

extension UIViewControllerAnimatedTransitioning {

    public func transitionsViews(from transitionContext: UIViewControllerContextTransitioning) -> (container: UIView, from: UIView, to: UIView) {
        let toVC = transitionContext.viewController(forKey: .to)!
        let fromVC = transitionContext.viewController(forKey: .from)!
        let toView = toVC.view!
        let fromView = fromVC.view!
        let container = transitionContext.containerView

        return (container, fromView, toView)
    }
}
