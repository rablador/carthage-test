import Foundation

extension NumberFormatter {

    public static var percentFormatter: NumberFormatter {
        let formatter = NumberFormatter()
        formatter.numberStyle =  .percent
        formatter.multiplier = 1
        formatter.maximumFractionDigits = 1

        return formatter
    }
}
