import Foundation

extension UserDefaults {

    public func set(_ value: Any?, key: String) -> UserDefaults {
        set(value, forKey: key)
        return self
    }

    public func removeObject(key: String) -> UserDefaults {
        removeObject(forKey: key)
        return self
    }
}

