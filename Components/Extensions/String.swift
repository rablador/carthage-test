import UIKit

extension String {

    public var url: URL? {
        guard
            let url = URL(string: self),
            UIApplication.shared.canOpenURL(url)
            else { return nil }
        return url
    }
}
