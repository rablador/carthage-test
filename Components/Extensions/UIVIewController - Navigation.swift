import UIKit

extension UIViewController {

    public func dismissed<T>(completion: ((T) -> Void)? = nil) -> ValueCallback<T> {
        return { value in
            self.dismiss(animated: true) {
                completion?(value)
            }
        }
    }

    public func presented<T>(_ viewController: UIViewController, completion: ValueCallback<T>? = nil) -> ValueCallback<T> {
        return { value in
            completion?(value)
            self.present(viewController, animated: true) { }
        }
    }

    public func pushed<T>(_ viewController: UIViewController, completion: ValueCallback<T>? = nil) -> ValueCallback<T> {
        return { value in
            completion?(value)
            self.navigationController?.pushViewController(viewController, animated: true)
        }
    }
}
