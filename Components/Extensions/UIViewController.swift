import UIKit

extension UIViewController {

    public func addOverlay(viewController: UIViewController, container: UIView, fade: Bool = true, completion: Callback? = nil) {
        addChildViewController(viewController)
        viewController.didMove(toParentViewController: self)

        viewController.view.alpha = 0
        viewController.view.frame = container.frame
        container.addSubview(viewController.view)

        UIView.animate(withDuration: fade ? 0.2 : 0, animations: {
            viewController.view.alpha = 1
        }, completion: { _ in completion?() })
    }

    public func removeOverlay(viewController: UIViewController, fade: Bool = true, completion: Callback? = nil) {
        UIView.animate(withDuration: fade ? 0.2 : 0, animations: {
            viewController.view.alpha = 0
        }, completion: { _ in
            viewController.view.removeFromSuperview()
            viewController.removeFromParentViewController()

            completion?()
        })
    }

    public func childViewControllers<T>(ofType type: T.Type) -> [T] {
        return childViewControllers.flatMap { $0 as? T }
    }
}
