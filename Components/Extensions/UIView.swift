import UIKit

extension UIView {

    @IBInspectable public var cornerRadius: CGFloat {
        get { return layer.cornerRadius }
        set {
            layer.cornerRadius = newValue
            layer.masksToBounds = newValue > 0
        }
    }

    @IBInspectable public var borderWidth: CGFloat {
        get { return layer.borderWidth }
        set { layer.borderWidth = newValue }
    }

    @IBInspectable public var borderColor: UIColor {
        get {
            guard let borderColor = layer.borderColor else { return .black }
            return UIColor(cgColor: borderColor)
        }
        set { layer.borderColor = newValue.cgColor }
    }

    @IBInspectable public var shadowRadius: CGFloat {
        get { return layer.shadowRadius }
        set { layer.shadowRadius = newValue }
    }

    @IBInspectable public var shadowOpacity: Float {
        get { return layer.shadowOpacity }
        set { layer.shadowOpacity = newValue }
    }

    @IBInspectable public var shadowOffset: CGSize {
        get { return layer.shadowOffset }
        set { layer.shadowOffset = newValue }
    }

    @IBInspectable public var shadowColor: UIColor? {
        get {
            if let color = layer.shadowColor {
                return UIColor(cgColor: color)
            }
            return nil
        }
        set {
            if let color = newValue {
                layer.shadowColor = color.cgColor
            } else {
                layer.shadowColor = nil
            }
        }
    }
}

extension UIView {

    public var size: CGSize {
        get { return frame.size }
        set { frame.size = newValue }
    }

    public var origin: CGPoint {
        get { return frame.origin }
        set { frame.origin = newValue }
    }

    public var width: CGFloat {
        get { return frame.size.width }
        set { frame.size.width = newValue }
    }

    public var height: CGFloat {
        get { return frame.size.height }
        set { frame.size.height = newValue }
    }

    public var top: CGFloat {
        get { return frame.origin.y }
        set { frame.origin.y = newValue }
    }

    public var bottom: CGFloat {
        return top + height
    }

    public var left: CGFloat {
        get { return frame.origin.x }
        set { frame.origin.x = newValue }
    }

    public var right: CGFloat {
        return left + width
    }

    public func convertedFrame(for view: UIView, in container: UIView) -> CGRect {
        return container.convert(view.frame, to: self)
    }
}

extension UIView {

    public func addGradientLayer(startPoint: CGPoint, endPoint: CGPoint, startColor: UIColor, endColor: UIColor) {
        let gradientLayer = CAGradientLayer()

        gradientLayer.frame = frame
        gradientLayer.colors = [startColor.cgColor, endColor.cgColor]
        gradientLayer.startPoint = startPoint
        gradientLayer.endPoint = endPoint

        layer.addSublayer(gradientLayer)
    }
}

extension UIView {

    public var bundle: Bundle {
        return Bundle(for: type(of: self))
    }

    public func xibSetup(name: String) {
        guard let view = bundle.loadNibNamed(name, owner: self, options: nil)?.first as? UIView else { fatalError("Could not load nib named \(name).") }

        view.frame = bounds
        addSubview(view)
    }
}

extension UIView {

    public func fadeTransition(duration: Double = 0.2, completion: Callback? = nil) {
        CATransaction.begin()
        CATransaction.setCompletionBlock({ completion?() })

        let animation = CATransition()
        animation.timingFunction = CAMediaTimingFunction(name: kCAMediaTimingFunctionEaseInEaseOut)
        animation.type = kCATransitionFade
        animation.duration = duration

        layer.add(animation, forKey: kCATransitionFade)

        CATransaction.commit()
    }

    public func fadeIn(duration: Double = 0, alpha: Double = 1, completion: Callback? = nil) {
        setAlpha(alpha: alpha, animated: duration != 0, duration: duration, completion: completion)
    }

    public func fadeOut(duration: Double = 0, alpha: Double = 0, completion: Callback? = nil) {
        setAlpha(alpha: alpha, animated: duration != 0, duration: duration, completion: completion)
    }

    public func setAlpha(alpha: Double, animated: Bool, duration: Double = 0.2, completion: Callback? = nil) {
        if animated {
            UIView.animate(withDuration: duration, animations: {
                self.alpha = CGFloat(alpha)
            }, completion: { _ in completion?() })
        } else {
            self.alpha = CGFloat(alpha)
            completion?()
        }
    }
}

extension UIView {

    public func springAnimation(to destination: CGPoint, duration: Double, options: UIViewAnimationOptions = [], completion: Callback? = nil) {
        layer.removeAllAnimations()

        let destination = CGRect(origin: destination, size: frame.size)
        let animationOptions = options.union(.curveEaseInOut)

        UIView.animate(withDuration: duration, delay: 0, usingSpringWithDamping: 1, initialSpringVelocity: 0, options: animationOptions, animations: {
            self.frame = destination
        }, completion: { _ in completion?() })
    }
}
