import UIKit

extension UITableView {

    public func dequeueReusable<T: UITableViewCell>(cell type: T.Type, for indexPath: IndexPath) -> T {
        let instance = type.init()
        let identifier = String(describing: Swift.type(of: instance))
        return dequeueReusableCell(withIdentifier: identifier, for: indexPath) as! T
    }
}
