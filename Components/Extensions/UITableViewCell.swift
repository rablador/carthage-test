extension UITableViewCell: Interactable {

    public var isInteractable: Bool {
        get { return isUserInteractionEnabled }
        set {
            isUserInteractionEnabled = newValue
            alpha = newValue ? 1.0 : 0.5
        }
    }
}
