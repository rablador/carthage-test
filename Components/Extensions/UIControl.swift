extension UIControl: Interactable {

    public var isInteractable: Bool {
        get { return isEnabled }
        set {
            isEnabled = newValue
            alpha = newValue ? 1.0 : 0.5
        }
    }
}
