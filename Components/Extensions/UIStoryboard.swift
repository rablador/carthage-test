import UIKit

extension UIStoryboard {

    public func instantiate<T: UIViewController>(_ type: T.Type) -> T {
        let instance = type.init()
        let identifier = String(describing: Swift.type(of: instance))
        return instantiateViewController(withIdentifier: identifier) as! T
    }
}
