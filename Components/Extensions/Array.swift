import Foundation

extension Array {

    public func filterDuplicatesOnAttribute(includeElement: @escaping (_ lhs: Element, _ rhs: Element) -> Bool) -> [Element] {
        var results = [Element]()

        forEach { (element) in
            let existingElements = results.filter {
                return includeElement(element, $0)
            }
            if existingElements.count == 0 {
                results.append(element)
            }
        }

        return results
    }
}

extension Array where Element: Interactable {

    public func enable() {
        setEnable(elements: self, to: true)
    }

    public func disable() {
        setEnable(elements: self, to: false)
    }

    private func setEnable(elements: [Interactable], to isEnabled: Bool) {
        elements.forEach { element in
            element.isInteractable = isEnabled
        }
    }
}
