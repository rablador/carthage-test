import UIKit

extension UIViewController {

    public typealias APIMethod<T> = (@escaping ResultHandler<T>) -> Void
    public typealias APIMethodArgument<T, U> = (T, @escaping ResultHandler<U>) -> Void

    public func setupNumberPad(for textField: UITextField) {
        textField.keyboardType = .numberPad
        textField.addDoneButton()
    }

    public func handle<T, U: Interactable>(loadingView: LoadingView, controls: [U], for apiMethod: APIMethod<T>, completion: @escaping ResultHandler<T>) {
        loadingView.startAnimating()
        controls.disable()
        apiMethod(endAnimation(for: loadingView, enable: controls, completion: completion))
    }

    public func handle<T, U, V: Interactable>(loadingView: LoadingView, controls: [V], for apiMethod: APIMethodArgument<T, U>, with item: T, completion: @escaping ResultHandler<U>) {
        loadingView.startAnimating()
        controls.disable()
        apiMethod(item, endAnimation(for: loadingView, enable: controls, completion: completion))
    }

    public func endAnimation<T, U: Interactable>(for loadingView: LoadingView, enable controls: [U], completion: @escaping ResultHandler<T>) -> ResultHandler<T> {
        return { result in
            loadingView.stopAnimating()
            controls.enable()
            completion(result)
        }
    }
}
