import Foundation

extension Data {

    public func toJsonDictionary() throws -> JSONDictionary {
        return try JSONSerialization.jsonObject(with: self, options: []) as! JSONDictionary
    }
}
