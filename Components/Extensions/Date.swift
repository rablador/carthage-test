import Foundation

extension Date {

    public func inFuture(mins: Int) -> Date {
        return addingTimeInterval(Double(mins) * 60)
    }
}
