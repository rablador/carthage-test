public struct APIConfig {

    public static let shared = APIConfig()

    public var production: Server?
    public var test: Server?
}
