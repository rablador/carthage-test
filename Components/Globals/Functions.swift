import Foundation

public func isDebug() -> Bool {
    #if DEBUG
        return true
    #else
        return false
    #endif
}

public func passData<T: DataTransferable>(to viewController: T) -> ValueCallback<T.Data> where T: UIViewController {
    return { data in
        var mutableVC = viewController
        mutableVC.data = data
    }
}
