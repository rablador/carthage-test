import UIKit

public class SingleChoiceExpandableTableViewDelegate: NSObject, UITableViewDelegate {

    public func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        autoUpdate(tableView)
        tableView.scrollToNearestSelectedRow(at: .none, animated: true)
    }

    public func tableView(_ tableView: UITableView, willSelectRowAt indexPath: IndexPath) -> IndexPath? {
        let alreadySelected = tableView.indexPathForSelectedRow == indexPath

        if alreadySelected {
            tableView.deselectRow(at: indexPath, animated: true)
            autoUpdate(tableView)
            return nil
        }

        return indexPath
    }

    public func autoUpdate(_ tableView: UITableView) {
        tableView.beginUpdates()
        tableView.endUpdates()
    }
}
