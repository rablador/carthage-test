import UIKit

public class NumberTextFieldDelegate: RefillTextFieldDelegate {

    public var lengthLimit: Int?
    public var didEndEditing: ValueCallback<UITextField>?
    public var shouldRefill = true

    public func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        let validLength = isValidLength(text: "\(textField.text!)\(string)")
        let isNumber = Int(string) != nil
        let isCorrectEnteredString = string.isEmpty || (isNumber && validLength)
        return isCorrectEnteredString
    }

    public func textFieldDidEndEditing(_ textField: UITextField) {
        didEndEditing?(textField)
    }

    override public func textFieldShouldEndEditing(_ textField: UITextField) -> Bool {
        return shouldRefill ? super.textFieldShouldEndEditing(textField) : true
    }

    private func isValidLength(text: String) -> Bool {
        return lengthLimit != nil ? text.count <= lengthLimit! : true
    }
}
