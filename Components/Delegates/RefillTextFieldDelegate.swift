import UIKit

public class RefillTextFieldDelegate: NSObject, UITextFieldDelegate {

    public var textBeforeEdit: String?

    public func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        textBeforeEdit = textField.text
        return true
    }

    public func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }

    public func textFieldShouldEndEditing(_ textField: UITextField) -> Bool {
        if textField.text!.isEmpty {
            textField.text = textBeforeEdit
        }
        return true
    }
}
